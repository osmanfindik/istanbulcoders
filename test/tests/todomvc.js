/* globals gauge*/
"use strict";
const { openBrowser, closeBrowser, goto, focus, write, click, button, text, textBox, exists } = require('taiko');
const assert = require("assert");

beforeSuite(async () => {
    await openBrowser({args: [
      '--headless',
      '--disable-gpu',
      '--disable-dev-shm-usage',
      '--disable-setuid-sandbox',
      '--no-first-run',
      '--no-sandbox',
      '--no-zygote']})
});

afterSuite(async () => {
    await closeBrowser();
});

step("Initial page", async function() {
	await goto("http://<TEST_IP>:9999");
        await button('Yeni Not Ekle').exists();
});

step("Note add", async function() {
        await goto("http://<TEST_IP>:9999");
        await focus(textBox(''));
        await write("sample note");
        await click("Yeni Not Ekle");
        await text("sample note").exists();
});

step("Note add delete", async function() {
        await goto("http://<TEST_IP>:9999");
	await focus(textBox(''));
	await write("sample note");
	await click("Yeni Not Ekle");
	await button('Notu Sil').exists();
	await text("sample note").exists();
	await click ("Notu Sil");
});

