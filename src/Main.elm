module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)


-- MAIN


main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
    { toDoItemList : List ToDoItem
    , newItemText : String
    , newItemId : Int
    }


type alias ToDoItem =
    { toDoItemId : Int
    , toDoItemText : String
    }


newToDoItem : String -> Int -> ToDoItem
newToDoItem text id =
    { toDoItemId = id
    , toDoItemText = text
    }


init : Model
init =
    Model [] "" 1



-- UPDATE


type Msg
    = Add
    | Delete Int
    | ToDoItemClicked String


update : Msg -> Model -> Model
update msg model =
    case msg of
        Add ->
            { model
                | toDoItemList =
                    if String.isEmpty model.newItemText then
                        model.toDoItemList
                    else
                        (newToDoItem model.newItemText model.newItemId) :: model.toDoItemList
                , newItemText = ""
                , newItemId = model.newItemId + 1
            }

        Delete id ->
            { model | toDoItemList = List.filter (\t -> t.toDoItemId /= id) model.toDoItemList }

        ToDoItemClicked itemText ->
            { model | newItemText = itemText }



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ viewAddToDo model
        , viewToDoList model
        ]


viewAddToDo : Model -> Html Msg
viewAddToDo model =
    div []
        [ input [ type_ "text", value model.newItemText, onInput ToDoItemClicked ] []
        , button [ onClick Add ] [ text "Yeni Not Ekle" ]
        ]


viewToDoList : Model -> Html Msg
viewToDoList model =
    div [ style "color" "green" ]
        [ div [] [ renderList model.toDoItemList ]
        ]


renderList : List ToDoItem -> Html Msg
renderList list =
    ul []
        (List.map
            (\item ->
                li []
                    [ button [ onClick (Delete item.toDoItemId) ] [ text " Notu Sil" ]
                    , text "   "
                    , text item.toDoItemText
                    ]
            )
            list
        )
